#!/bin/bash

# Start BIND9
/usr/sbin/named -u bind
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start BIND9: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if process exited.
# The container exits with an error if it detects that process has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
  ps aux |grep named |grep -q -v grep
  NAMED_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  if [ $NAMED_STATUS -ne 0 ]; then
    echo "named service has already exited."
    exit 1
  fi
done