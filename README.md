# **BIND9 DNS Server test as Docker container Service**

**To test this container, you can run this command:**  
`docker run -it --rm --name dns-test -p 53:53/tcp -p 53:53/udp registry.gitlab.com/hansom1/test`

After running container you may test working the service. For example:  
`nslookup vasya.test.ru 127.0.0.1`  
`nslookup test.test.ru 127.0.0.1`   
`nslookup subdomain.test.test.ru 127.0.0.1`  
`nslookup ya.ru 127.0.0.1`  
`nslookup 87.250.250.242 127.0.0.1`  
`nslookup prod.ext.test.ru 127.0.0.1`  
`nslookup 137.221.106.104 127.0.0.1`

**Or clone this git to your local docker machine and take this steps:**  
To build image use:  
`docker build -t dns-server-test:latest .`

To run test container use:  
`docker run -it --rm --name dns-test -p 53:53/tcp -p 53:53/udp dns-server-test`

Also you may change lines 8 and 9 in Dockerfile, looks as:  
`COPY ./test.csv .`  
`RUN python3 genconfig.py test.csv`

And may change domain and name server name in script genconfig.py:  
`# Domain parameters`  
`root_domain = 'test.ru'`  
`name_server = 'ns'`  
`forwarders = ('8.8.8.8', '8.8.4.4', '212.48.193.36')`

