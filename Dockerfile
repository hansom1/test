# Build configuration files
FROM ubuntu:16.04 AS build
RUN apt-get update \
  && apt-get install -y python3

WORKDIR /gencfg
COPY ./genconfig.py .
COPY ./test.csv .
RUN python3 genconfig.py test.csv

# Final stage
FROM ubuntu:16.04
RUN apt-get update \
  && apt-get install -y bind9

WORKDIR /etc/bind
COPY --from=build /gencfg/config/. ./
COPY ./bindstart.sh /usr/bin/.
RUN chmod 755 /usr/bin/bindstart.sh

EXPOSE 53/tcp
EXPOSE 53/udp

ENTRYPOINT ["/usr/bin/bindstart.sh"]
