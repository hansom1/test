import csv, sys, os
from string import Template

# Domain parameters
root_domain = 'test.ru'
name_server = 'ns'
forwarders = ('8.8.8.8', '8.8.4.4', '212.48.193.36')

# File content templates
zone_template = ''';
; $comment
;
$TTL	604800
@	IN	SOA	$ns.$rootdomain. root.$rootdomain. (
			      $serial		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@			IN	NS	$ns.$rootdomain.

$records'''
options_template = '''options {
	directory "/var/cache/bind";

	// If there is a firewall between you and nameservers you want
	// to talk to, you may need to fix the firewall to allow multiple
	// ports to talk.  See http://www.kb.cert.org/vuls/id/800113

	// If your ISP provided one or more IP addresses for stable 
	// nameservers, you probably want to use them as forwarders.  
	// Uncomment the following block, and insert the addresses replacing 
	// the all-0's placeholder.

	forwarders {
		$dnsforwarders
	};

	//========================================================================
	// If BIND logs error messages about the root key being expired,
	// you will need to update your keys.  See https://www.isc.org/bind-keys
	//========================================================================
	dnssec-validation auto;

	auth-nxdomain no;    # conform to RFC1035
	listen-on-v6 { any; };
};'''
local_template = '''//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";

$includes'''

# Internal variables
zones = list()


def reverse_ipv4(ip, parts=4):
    out = list(str(ip).split('.'))
    out.reverse()
    return '.'.join(out[:parts])


def read_config(file):
    nslist = list()
    try:
        with open(file) as cfgfile:
            nsconfig = list(csv.reader(cfgfile, dialect='excel', delimiter=','))
            for row in nsconfig[1:]:
                names = [name.strip() for name in list(str(row[1]).split(';'))]
                nslist.append(dict(ip=str(row[0]).strip(), names=names, comment=str(row[2]).strip()))
    except FileNotFoundError:
        print('{0} file not found'.format(file))
        exit(1)
    except IOError:
        print('{0} file not accessible'.format(file))
        exit(4)
    return nslist


def gen_domain_a(names_list):
    file = 'config/db.' + root_domain
    try:
        with open(file, 'w') as afile:
            arecords = "; Addresses\n"
            for rec in names_list:
                for name in rec['names']:
                    arecords += name.ljust(20) + " IN A " + rec['ip'].ljust(15) + " ; " + rec['comment'] + "\n"
            tout = Template(zone_template)
            out = tout.safe_substitute(ns=name_server, rootdomain=root_domain, records=arecords,
                                       comment='BIND data file for domain ' + root_domain, serial=2)
            print(out, file=afile, end='')
    except IOError:
        print('error creation of {0} file'.format(file))
        exit(4)
    zones.append(dict(name=root_domain, file='db.' + root_domain, reverse=False))


def gen_rev_local(names_list):
    names_local = [name for name in names_list if name['ip'].startswith('192.168.')]
    file = 'config/db.168.192'
    try:
        with open(file, 'w') as pfile:
            ptrrecords = "; Addresses\n"
            for rec in names_local:
                ptrrecords += reverse_ipv4(rec['ip'], parts=2).ljust(10) + " IN PTR " + str(
                    rec['names'][0] + '.' + root_domain + ".").ljust(20) + " ; " + rec['comment'] + "\n"
            tout = Template(zone_template)
            out = tout.safe_substitute(ns=name_server, rootdomain=root_domain, records=ptrrecords,
                                       comment='BIND reverse data file for domain ' + root_domain, serial=1)
            print(out, file=pfile, end='')
    except IOError:
        print('error creation of {0} file'.format(file))
        exit(4)
    zones.append(dict(name='168.192.in-addr.arpa', file='db.168.192', reverse=True))


def gen_rev_ext(names_list):
    names_ext = [name for name in names_list if not name['ip'].startswith('192.168.')]
    for rec in names_ext:
        file = 'config/db.' + reverse_ipv4(rec['ip'])
        try:
            with open(file, 'w') as pfile:
                ptrrecords = "@ IN PTR " + str(rec['names'][0] + '.' + root_domain + ".").ljust(20) + " ; " + rec[
                    'comment'] + "\n"
                tout = Template(zone_template)
                out = tout.safe_substitute(ns=name_server, rootdomain=root_domain, records=ptrrecords,
                                           comment='BIND reverse data file for domain ' + root_domain, serial=1)
                print(out, file=pfile, end='')
        except IOError:
            print('error creation of {0} file'.format(file))
            exit(4)
        zones.append(
            dict(name=reverse_ipv4(rec['ip']) + '.in-addr.arpa', file='db.' + reverse_ipv4(rec['ip']), reverse=True))


def gen_zones(path='/etc/bind/'):
    file = 'config/zones.' + root_domain
    try:
        with open(file, 'w') as zfile:
            for zone in zones:
                if zone['reverse']:
                    print(
                        'zone "' + zone['name'] + '" { type master; notify no; file "' + path + zone['file'] + '"; };',
                        file=zfile)
                else:
                    print('zone "' + zone['name'] + '" { type master; file "' + path + zone['file'] + '"; };',
                          file=zfile)
    except IOError:
        print('error creation of {0} file'.format(file))
        exit(4)


def gen_options():
    file = 'config/named.conf.options'
    tout = Template(options_template)
    out = tout.safe_substitute(dnsforwarders=";\n\t\t".join(forwarders) + ';')
    try:
        with open(file, 'w') as ofile:
            print(out, file=ofile)
    except IOError:
        print('error creation of {0} file'.format(file))
        exit(4)


def gen_local(path='/etc/bind/'):
    file = 'config/named.conf.local'
    tout = Template(local_template)
    out = tout.safe_substitute(includes='include "' + path +'zones.' + root_domain + '";')
    try:
        with open(file, 'w') as lfile:
            print(out, file=lfile)
    except IOError:
        print('error creation of {0} file'.format(file))
        exit(4)


if __name__ == '__main__':
    print("DNS (BIND9) Server configuration generator\n")
    if len(sys.argv) == 2:
        print("Start generation...")
        print("Reading domain names from file {0}...".format(sys.argv[1]), end='')
        nslist = read_config(sys.argv[1])
        print("OK")
        if not os.path.isdir('config'):
            os.mkdir('config')
        print("Generate zone A records file...", end='')
        gen_domain_a(nslist)
        print("OK")
        print("Generate reverse DNS lookup file for local net...", end='')
        gen_rev_local(nslist)
        print("OK")
        print("Generate reverse DNS lookup files for external network addresses...", end='')
        gen_rev_ext(nslist)
        print("OK")
        print("Generate zones list file...", end='')
        gen_zones()
        print("OK")
        print("Generate 'named.conf.options' file...", end='')
        gen_options()
        print("OK")
        print("Generate 'named.conf.local' file...", end='')
        gen_local()
        print("OK")
        print("\nAll done!")
    else:
        print("Using: genconfig.py nameslistfile.csv")
